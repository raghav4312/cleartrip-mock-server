package com.cleartrip.mockserver.ctmockserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CtMockserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(CtMockserverApplication.class, args);
	}

}
